package com.mands.test.movies.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mands.test.movies.MoviesApp;
import com.mands.test.movies.R;
import com.mands.test.movies.Utils;
import com.mands.test.movies.adapter.CollectionListAdapter;
import com.mands.test.movies.api.ApiService;
import com.mands.test.movies.model.Movie;
import com.mands.test.movies.model.NowPlaying;
import com.mands.test.movies.presenter.DetailPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class DetailActivity extends AppCompatActivity implements DetailPresenter.View, CollectionListAdapter.OnListItemClickedCallback {

    private static final String ARG_NOW_PLAYING = "ARG_NOW_PLAYING";

    public static Intent newIntent(Context context, NowPlaying nowPlaying){
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(ARG_NOW_PLAYING, nowPlaying);
        return  intent;
    }

    private PublishSubject<Integer> onCollectionClickedPublish = PublishSubject.create();

    @Inject
    ApiService apiService;

    private DetailPresenter presenter;

    @BindView(R.id.movie_title)
    TextView movieTitle;
    @BindView(R.id.movie_desc)
    TextView movieDesc;
    @BindView(R.id.collection_name)
    TextView collectionName;
    @BindView(R.id.movie_image)
    ImageView movieImage;
    @BindView(R.id.collection_list)
    RecyclerView collectionList;
    @BindView(R.id.loading_progress)
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        ((MoviesApp)getApplication()).getAppComponent().inject(this);

        NowPlaying nowPlaying;
        Intent intent = getIntent();

        if(intent.hasExtra(ARG_NOW_PLAYING)){
            nowPlaying = (NowPlaying) intent.getSerializableExtra(ARG_NOW_PLAYING);
        } else {
            throw new RuntimeException("Must provide a NowPlaying");
        }

        presenter = new DetailPresenter(nowPlaying, apiService);
        presenter.attach(this);
    }

    @Override
    public Observable<Integer> onCollectionItemClicked() {
        return onCollectionClickedPublish;
    }

    @Override
    public void updateView(Movie movie) {
        Utils.loadImage(this, movie.getPosterPath(), movieImage);
        movieTitle.setText(movie.getTitle());
        movieDesc.setText(movie.getOverview());
        if(movie.getBelongsToCollection() != null)
            collectionName.setText(movie.getBelongsToCollection().getName());
    }

    @Override
    public void updateCollections(List<Movie> movies) {
        CollectionListAdapter adapter = new CollectionListAdapter(movies, this);
        collectionList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        collectionList.setAdapter(adapter);
    }

    @Override
    public void initialiseView(NowPlaying nowPlaying) {
        movieTitle.setText(nowPlaying.getTitle());
        movieDesc.setText(nowPlaying.getOverview());
        Utils.loadImage(this, nowPlaying.getPosterPath(), movieImage);
    }

    @Override
    public void showLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }


    @Override
    protected void onDestroy() {
        presenter.detach(this);
        super.onDestroy();
    }

    @Override
    public void showError(String errorResponse) {

    }

    @Override
    public void onItemClicked(int position) {
        onCollectionClickedPublish.onNext(position);
    }
}
