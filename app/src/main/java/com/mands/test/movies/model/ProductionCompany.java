package com.mands.test.movies.model;

import com.google.gson.annotations.SerializedName;

public class ProductionCompany {

    @SerializedName("name")
    public String name;
    @SerializedName("id")
    public int id;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
