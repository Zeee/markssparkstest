package com.mands.test.movies.presenter;


import com.mands.test.movies.api.ApiService;
import com.mands.test.movies.model.Movie;
import com.mands.test.movies.model.NowPlaying;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class DetailPresenter extends Presenter<DetailPresenter.View> {

    private View view;
    private NowPlaying nowPlaying;
    private List<Movie> collection;
    private ApiService apiService;

    public DetailPresenter(NowPlaying nowPlaying, ApiService apiService) {
        this.nowPlaying = nowPlaying;
        this.apiService = apiService;
    }

    @Override
    protected void onViewAttached(View view) {
        super.onViewAttached(view);
        this.view = view;

        addSubscription(view.onCollectionItemClicked()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer position) throws Exception {
                        getMovieInfo(collection.get(position).getId());
                    }
                }));

        view.initialiseView(nowPlaying);
        getMovieInfo(nowPlaying.getId());
    }

    @Override
    protected void onViewDetached(View view) {
        super.onViewDetached(view);
        this.view = null;
    }

    private void getMovieInfo(final int movieId){
        view.showLoading(true);
        apiService.getMovieInformation(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<Movie>() {
                    @Override
                    public void onNext(Movie movie) {
                        view.updateView(movie);
                        if(movie.getBelongsToCollection() != null)
                            getCollectionInformation(movie.getBelongsToCollection().getId());
                    }

                    @Override
                    public void onError(Throwable t) {
                        view.showError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void getCollectionInformation(int collectionId){
        apiService.getCollectionInfo(collectionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<List<Movie>>() {
                    @Override
                    public void onNext(List<Movie> movies) {
                        DetailPresenter.this.collection = movies;
                        view.updateCollections(movies);
                    }

                    @Override
                    public void onError(Throwable t) {
                        view.showError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.showLoading(false);
                    }
                });
    }



    public interface View extends Presenter.View {
        Observable<Integer> onCollectionItemClicked();
        void updateView(Movie movie);
        void updateCollections(List<Movie> movies);
        void initialiseView(NowPlaying nowPlayingList);
        void showLoading(boolean isLoading);
    }
}
