package com.mands.test.movies.api;


import com.google.gson.annotations.SerializedName;
import com.mands.test.movies.model.MovieDates;
import com.mands.test.movies.model.NowPlaying;

import java.util.List;

public class NowPlayingResponse {

    @SerializedName("results")
    private List<NowPlaying> results = null;
    @SerializedName("page")
    private Integer page;
    @SerializedName("total_results")
    private Integer totalResults;
    @SerializedName("dates")
    private MovieDates dates;
    @SerializedName("total_pages")
    private Integer totalPages;

    public List<NowPlaying> getResults() {
        return results;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public MovieDates getDates() {
        return dates;
    }

    public Integer getTotalPages() {
        return totalPages;
    }
}

