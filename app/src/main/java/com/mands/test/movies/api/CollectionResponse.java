package com.mands.test.movies.api;


import com.google.gson.annotations.SerializedName;
import com.mands.test.movies.model.Movie;

import java.util.List;

public class CollectionResponse {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("overview")
    private String overview;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("parts")
    List<Movie> moviesInCollection;

    public List<Movie> getMoviesInCollection() {
        return moviesInCollection;
    }
}
