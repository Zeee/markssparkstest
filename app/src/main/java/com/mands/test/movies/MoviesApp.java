package com.mands.test.movies;

import android.app.Application;

import timber.log.Timber;

public class MoviesApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        appComponent = DaggerAppComponent.builder()
                .networkModule(new NetworkModule(BuildConfig.BASE_URL, BuildConfig.API_KEY))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
