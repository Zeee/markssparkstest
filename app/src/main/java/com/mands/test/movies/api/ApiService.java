package com.mands.test.movies.api;


import com.mands.test.movies.model.Movie;
import com.mands.test.movies.model.NowPlaying;

import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

public class ApiService {

    private MovieService movieService;
    private String apiKey;

    public ApiService(String apiKey, MovieService movieService) {
        this.movieService = movieService;
        this.apiKey = apiKey;
    }

    public Flowable<List<NowPlaying>> getListOfNowPlayingMovies(int pageNumber){
        return movieService.getNowPlayingList(pageNumber, apiKey)
                .map(new Function<NowPlayingResponse, List<NowPlaying>>() {
                    @Override
                    public List<NowPlaying> apply(NowPlayingResponse nowPlayingResponse) throws Exception {
                        return nowPlayingResponse != null ? nowPlayingResponse.getResults() : Collections.<NowPlaying>emptyList();
                    }
                });
    }


    public Flowable<Movie> getMovieInformation(int movieId){
        return movieService.getMovieInfo(movieId, apiKey);
    }

    public Flowable<List<Movie>> getCollectionInfo(int movieId){
        return movieService.getCollectionInfo(movieId, apiKey)
                .map(new Function<CollectionResponse, List<Movie>>() {
                    @Override
                    public List<Movie> apply(CollectionResponse collectionResponse) throws Exception {
                        return collectionResponse != null ? collectionResponse.getMoviesInCollection() : Collections.<Movie>emptyList();
                    }
                });
    }

}
