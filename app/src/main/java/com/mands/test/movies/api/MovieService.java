package com.mands.test.movies.api;


import com.mands.test.movies.model.Movie;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieService {

    @GET("movie/now_playing/")
    Flowable<NowPlayingResponse> getNowPlayingList(@Query("page") int page, @Query("api_key") String apiKey);

    @GET("movie/{movie_id}")
    Flowable<Movie> getMovieInfo(@Path("movie_id") int movieId, @Query("api_key") String apiKey);

    @GET("collection/{collection_id}")
    Flowable<CollectionResponse> getCollectionInfo(@Path("collection_id") int collectionId, @Query("api_key") String apiKey);

}
