package com.mands.test.movies.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mands.test.movies.MoviesApp;
import com.mands.test.movies.R;
import com.mands.test.movies.adapter.MovieListAdapter;
import com.mands.test.movies.api.ApiService;
import com.mands.test.movies.model.NowPlaying;
import com.mands.test.movies.presenter.MovieListPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class MovieListActivity extends AppCompatActivity implements MovieListPresenter.View, MovieListAdapter.OnListItemClickedCallback {

    private PublishSubject<Object> moreMoviesPublish = PublishSubject.create();
    private PublishSubject<Integer> onMovieClickedPublish = PublishSubject.create();

    private MovieListPresenter presenter;
    private GridLayoutManager gridLayoutManager;
    private boolean isLoading = false;

    @Inject
    ApiService apiService;

    @BindView(R.id.movie_list)
    RecyclerView movieList;

    private MovieListAdapter adapter;
    private int visibleItemCount,  totalItemCount, pastVisibleItems;
    private ProgressDialog progressDialog; //I understand this is deprecated. Using this for the sake of speed.

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        ButterKnife.bind(this);

        ((MoviesApp)getApplication()).getAppComponent().inject(this);

        //Ideally this would be a view and it would not block the UI.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.loading));

        presenter = new MovieListPresenter(apiService);
        presenter.attach(this);
    }

    @Override
    protected void onDestroy() {
        presenter.detach(this);
        super.onDestroy();
    }

    @Override
    public void initialiseList(List<NowPlaying> nowPlayingList) {
        adapter = new MovieListAdapter(nowPlayingList, this);
        gridLayoutManager = new GridLayoutManager(this, 2);
        movieList.setLayoutManager(gridLayoutManager);
        //Create infinite scroll listener.
        movieList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisibleItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if(!isLoading){
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            moreMoviesPublish.onNext(new Object());
                        }
                    }
                }
            }
        });

        movieList.setAdapter(adapter);

        //Runnable used as a temp measure as onNext would not always publish. Could not figure out
        //what was wrong at this time.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moreMoviesPublish.onNext(new Object());
            }
        }, 300);

    }

    @Override
    public Observable<Object> onRequestMoreMovies() {
        return moreMoviesPublish;
    }

    @Override
    public Observable<Integer> onMovieClicked() {
        return onMovieClickedPublish;
    }

    @Override
    public void updateList(int oldCount, int newItemsCount) {
        adapter.notifyItemRangeInserted(oldCount, newItemsCount);
    }

    @Override
    public void showLoading(boolean isLoading) {
        this.isLoading = isLoading;
        if(isLoading){
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showDetailView(NowPlaying nowPlaying) {
        startActivity(DetailActivity.newIntent(this, nowPlaying));
    }

    @Override
    public void onItemClicked(int position) {
        onMovieClickedPublish.onNext(position);
    }

    @Override
    public void showError(String errorResponse) {
        //TODO
    }
}
