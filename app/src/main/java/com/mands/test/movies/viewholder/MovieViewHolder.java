package com.mands.test.movies.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mands.test.movies.R;
import com.mands.test.movies.Utils;
import com.mands.test.movies.model.NowPlaying;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.movie_image)
    ImageView movieImage;
    @BindView(R.id.movie_title)
    TextView movieTitle;
    private OnItemClickCallback callback;

    public MovieViewHolder(View itemView, OnItemClickCallback clickCallback) {
        super(itemView);
        callback = clickCallback;
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    public void bind(NowPlaying nowPlaying){
        if(nowPlaying != null){
            movieTitle.setText(nowPlaying.getTitle());
            Utils.loadImage(itemView.getContext(), nowPlaying.getPosterPath(), movieImage);
        }
    }

    @Override
    public void onClick(View v) {
        if(callback!=null){
            callback.onItemClicked(getAdapterPosition());
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(int position);
    }
}
