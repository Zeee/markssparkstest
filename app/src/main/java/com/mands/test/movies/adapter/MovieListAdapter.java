package com.mands.test.movies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mands.test.movies.viewholder.MovieViewHolder;
import com.mands.test.movies.R;
import com.mands.test.movies.model.NowPlaying;

import java.util.List;


public class MovieListAdapter extends RecyclerView.Adapter<MovieViewHolder> implements MovieViewHolder.OnItemClickCallback {

    private List<NowPlaying> nowPlayingList;
    private OnListItemClickedCallback onListItemClickedCallback;

    public MovieListAdapter(List<NowPlaying> nowPlayingList, OnListItemClickedCallback onListItemClickedCallback) {
        this.nowPlayingList = nowPlayingList;
        this.onListItemClickedCallback = onListItemClickedCallback;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_movie, parent, false);
        return new MovieViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(nowPlayingList.get(position));
    }

    @Override
    public int getItemCount() {
        return nowPlayingList == null ? 0 : nowPlayingList.size();
    }

    @Override
    public void onItemClicked(int position) {
        if(onListItemClickedCallback != null){
            onListItemClickedCallback.onItemClicked(position);
        }
    }

    public interface OnListItemClickedCallback {
        void onItemClicked(int position);
    }
}
