package com.mands.test.movies;

import com.mands.test.movies.activity.DetailActivity;
import com.mands.test.movies.activity.MovieListActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface AppComponent {
    void inject(MovieListActivity activity);
    void inject(DetailActivity activity);
}
