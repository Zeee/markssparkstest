package com.mands.test.movies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mands.test.movies.R;
import com.mands.test.movies.model.Movie;
import com.mands.test.movies.viewholder.CollectionViewHolder;

import java.util.List;


public class CollectionListAdapter extends RecyclerView.Adapter<CollectionViewHolder> implements CollectionViewHolder.OnItemClickCallback {

    private List<Movie> movies;
    private OnListItemClickedCallback onListItemClickedCallback;

    public CollectionListAdapter(List<Movie> movies, OnListItemClickedCallback onListItemClickedCallback) {
        this.movies = movies;
        this.onListItemClickedCallback = onListItemClickedCallback;
    }

    @Override
    public CollectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_collection, parent, false);
        return new CollectionViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(CollectionViewHolder holder, int position) {
        holder.bind(movies.get(position));
    }

    public int getItemCount() {
        return movies == null ? 0 : movies.size();
    }

    @Override
    public void onItemClicked(int position) {
        if(onListItemClickedCallback != null){
            onListItemClickedCallback.onItemClicked(position);
        }
    }

    public interface OnListItemClickedCallback {
        void onItemClicked(int position);
    }
}
