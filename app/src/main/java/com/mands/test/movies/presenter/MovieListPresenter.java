package com.mands.test.movies.presenter;


import com.mands.test.movies.api.ApiService;
import com.mands.test.movies.model.NowPlaying;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import timber.log.Timber;

public class MovieListPresenter extends Presenter<MovieListPresenter.View>{

    private View view;
    private ApiService apiService;
    private int currentPage;
    private List<NowPlaying> nowPlayingList;

    public MovieListPresenter(ApiService apiService) {
        this.apiService = apiService;
        this.currentPage = 1;
        this.nowPlayingList = new ArrayList<>();
    }

    @Override
    protected void onViewAttached(View view) {
        super.onViewAttached(view);
        this.view = view;

        addSubscription(view.onMovieClicked()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer position) throws Exception {
                        MovieListPresenter.this.view.showDetailView(nowPlayingList.get(position));
                    }
                }));

        addSubscription(view.onRequestMoreMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object unused) throws Exception {
                        fetchMoreMovies();
                    }
                }));

        view.initialiseList(nowPlayingList);
    }

    @Override
    protected void onViewDetached(View view) {
        super.onViewDetached(view);
        this.view = null;
    }


    private void fetchMoreMovies(){
        view.showLoading(true);
        apiService.getListOfNowPlayingMovies(currentPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<List<NowPlaying>>() {
                    @Override
                    public void onNext(List<NowPlaying> nowPlayings) {
                        currentPage++;
                        int oldCount = nowPlayingList.size();
                        int newItemCount = nowPlayings.size();
                        nowPlayingList.addAll(nowPlayings);
                        view.updateList(oldCount, newItemCount);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t);
                    }

                    @Override
                    public void onComplete() {
                        Timber.i("Done");
                        view.showLoading(false);
                    }
                });
    }


    public interface View extends Presenter.View {
        Observable<Object> onRequestMoreMovies();
        Observable<Integer> onMovieClicked();
        void initialiseList(List<NowPlaying> nowPlayingList);
        void updateList(int oldCount, int newItemsCount);
        void showLoading(boolean isLoading);
        void showDetailView(NowPlaying nowPlaying);
    }
}
