# README #

This project is my solution to the technical test. Additionally I would have done the following: 

* I would have liked to have used some of the Material Design support library features like parallax scrolling, ripples effects and Activity transitions.

* For the unit tests I would hace created a mock `ApiService` rather than hard cording the expected responses.

* UI tests would have been implemented using the `Espresso` testing framework.

* Also I would have modified the Gradle script to contain dev, test and prod flavours.